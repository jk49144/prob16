function init(localID, taskConstants){

}

function drawLineSegment(ctx, line) {
    ctx.beginPath();
    ctx.moveTo(line.x0, line.y0);
    ctx.lineTo(line.x1, line.y1);
    ctx.stroke();
    ctx.fillText(i + 1, line.x0 - 5, line - 5);
    ctx.beginPath();
    ctx.ellipse(line.x0, line.y0, 2, 2, 0, 0, 2 * Math.PI);
    ctx.ellipse(line.x1, line.y1, 2, 2, 0, 0, 2 * Math.PI);
    ctx.fill();
}

function drawArrow(ctx, line) {
    let basept = [(line.x0 + line.x1) / 2,(line.y0 + line.y1) / 2];
    let dvect = [line.x1 - line.x0, line.y1 - line.y0];
    let dvectNorm = Math.sqrt(dvect[0]*dvect[0]+dvect[1]*dvect[1]);
    let vlen = 30, arlen = 8, arwing = 5;
    dvect[0] /= dvectNorm;
    dvect[1] /= dvectNorm;
    let artippt = [basept[0] + line.unitNormal[0] * vlen, basept[1] + line.unitNormal[1] * vlen];
    let arbasept = [basept[0] + line.unitNormal[0] * (vlen-arlen), basept[1] + line.unitNormal[1] * (vlen - arlen)];
    ctx.beginPath();
    ctx.moveTo(basept[0], basept[1]);
    ctx.lineTo(artippt[0], artippt[1]);
    ctx.moveTo(artippt[0], artippt[1]);
    ctx.lineTo(arbasept[0]+dvect[0]*arwing, arbasept[1]+dvect[1]*arwing);
    ctx.moveTo(artippt[0], artippt[1]);
    ctx.lineTo(arbasept[0]-dvect[0]*arwing, arbasept[1]-dvect[1]*arwing);
    ctx.stroke();
    if(line.segname) {
        var angle = Math.atan2(line.unitNormal[1], line.unitNormal[0]) + Math.PI/2;
        ctx.save();
        ctx.fillStyle = '#000000';
        ctx.font = '12px sans';
        ctx.textAlign = 'center';
        ctx.translate(basept[0] + line.unitNormal[0] *arlen / 2 - 10 * line.unitNormal[1], basept[1] + line.unitNormal[1] * 
                     arlen / 2 + 10 * line.unitNormal[0]);
        ctx.rotate(angle);
        ctx.fillText(line.myLength(), 0,0);
        ctx.restore();
    }
}

function drawInterceptions(lines, numOfLines) {
    for (i = 0; i < numOfLines; i++) {
        for (j = 0 ; j < numOfLines; j++) {
            if (i !== j) {
                
            }
        }
    }
}
