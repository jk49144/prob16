/*@#include#(serverUtil.js)*/
// Ova je metoda namijenjena inicijalizaciji zadatka.
// Dobiva questionConfigData koji postavlja editor konfiguracije
// -------------------------------------------------------------------------------------------------
function questionInitialize(questionConfig) {
    //init task configuration (see: config.json file for details)
    let myWidth  = questionConfig.canWidth;
    var a = 8, b = 7;
    //holding numbers of lines that are drawn on canvas.
    let numberOfLines = Math.floor(Math.random()*(questionConfig.maxNumOfLines - questionConfig.minNumOfLines)) 
                + questionConfig.minNumOfLines;
    //console.log(numberOfLines);
    let lines = [];
    
    for (i = 0; i < numberOfLines; i++) {
        let x0 = Math.floor(Math.random() * (questionConfig.canWidth - 2 * questionConfig.maxHorizontalLength)) 
                + questionConfig.maxHorizontalLength;

        var randomForX = Math.floor(Math.random() * 2);
        let x1;
        if (randomForX === 0) {
            x1 = x0 - (Math.floor(Math.random() * (questionConfig.maxHorizontalLength - questionConfig.minHorizontalLength)) + 
                    questionConfig.minHorizontalLength);
  
        }
        else {
            x1 = x0 - (Math.floor(Math.random() * (questionConfig.maxHorizontalLength - questionConfig.minHorizontalLength)) + 
                    questionConfig.minHorizontalLength);
        }
        var randomForY = Math.floor(Math.random() * 2);
        let y0 = Math.floor(Math.random() * (questionConfig.canHeight - 2 * questionConfig.maxVerticalLength))
                    questionConfig.maxVerticalLength;
        let y1;
        if (randomForY === 0) {
            y1 = y0 - (Math.floor(Math.random() * (questionConfig.maxVerticalLength - questionConfig.minVerticalLength)) + 
                    questionConfig.minVerticalLength);
        }
        else {
            y1 = y0 + (Math.floor(Math.random() * (questionConfig.maxVerticalLength - questionConfig.minVerticalLength)) + 
                    questionConfig.minVerticalLength);

        }
        let coef = [y0 - y1, x1 - x0, x0 * y1 - x1 * y0];
        let norm = Math.sqrt(coef[0] * coef[0] + coef[1] * coef[1]);
        var unitNormal = [coef[0] / norm, coef[1] / norm];
        var line = {
            x0 : x0, 
            y0:y0, 
            x1:x1, 
            y1:y1, 
            segname: i + 1, 
            coef: coef, 
            unitNormal: unitNormal,
            segLength : function() {
                //let dx = this.x1 - this.x0;
                //let dy = this.y1 - this.y0;
                //return Math.sqrt(dx * dx + dy * dy);
                return "jurij";
            },
            myFunc: function(){
                return 5;
            }
        };

        
        lines[i] = line;
    }
    
    userData.question = {
        correct: calculateCorrectSolution(a, b),
        questionState: null,
        lines : lines,
        a:a,
        numberOfLines:numberOfLines,

    };
     
}

function getComputedProperties() {
    return {
        drawingLines: userData.question.lines, a:userData.question.a
    };

}

function questionEvaluate() {
    function setCorrectSolution() {
        userData.correctQuestionState = userData.question.correct;
    }
    let res =  {
        correctness: 0.0,
        solved: false
    };
    if (userData.question.questionState !== null) {
        res.solved = true;
        if (userData.question.correct === userData.question.questionState) {
            res.correctness = 1.0; 
        }
        else {

        }

        res.correctness = 1;

    }
    return res;

}
function exportCommonState() {
    return {
        questionState: userData.question.questionState,
        taskConstants: userData.question.taskConstants,
        drawingLines: userData.question.lines, a:userData.question.a,
        numberOfLines: userData.question.numberOfLines
    
    };
}

function exportUserState(stateType) {
    if(stateType === "USER") {
        return {questionState:userData.question.questionState};
    }
    else if (stateType === "CORRECT") {
        return {questionState:userData.correctQuestionState};
    }
    return {}; 
}

function exportEmptyState() {
    return {
        qs: '0'
    };
}

function importQuestionState(data) {
    userData.question.questionState = data;
}